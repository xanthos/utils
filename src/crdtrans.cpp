#include <iostream>
#include <cstring>
#include <vector>
#include <algorithm>
#include <fstream>
#include <map>
#include <cmath>
#include <cassert>

#include "ggeodesy/ellipsoid.hpp"
#include "ggeodesy/geodesy.hpp"
#include "ggeodesy/car2ell.hpp"
#include "ggeodesy/ell2car.hpp"

using ngpt::ellipsoid;
using ngpt::Ellipsoid;

typedef
std::map<std::string, std::string> str_str_map;

// Constant value for π
constexpr double PI = M_PI;

// Dead simple Point class
// [x,y,z] are the initial(izing) coordinates (either cartesian in meters or
// ellipsoidal in radians and meters).
// [xr, yr, zr] are the transformed coordinates, again either cartesian or
// ellipsoidal in meters or radians.
// info holds any string(s) that should be reported at output (e.g. name/code).
// In case [x,y,z] or [xr,yr,zr] designate ellipsoidal coordinates, then the
// order is:
// x or xy -> longtitude (radians)
// y or yr -> latitude (radians)
// z or zr -> height (meters)
struct Point
{
  Point(double xx, double yy, double zz, const std::string& s="")
  noexcept
    : x(xx),
    y(yy),
    z(zz),
    info(s)
    {};
  double x, y, z;    ///< input coordinates
  double xr, yr, zr; ///< transformed coordinates
  std::string info;  ///< comment string
};

void
print_help() noexcept
{
  std::cout<<"\nUsage: crdtrans";
  std::cout<<"\n";
  std::cout<<"\nTrnasform (geocentric) cartesian coordinates to ellipsoidal or";
  std::cout<<"\nthe inverse. Input is read from STDIN or a file.";
  std::cout<<"\n";
  std::cout<<"\nOptions:";
  std::cout<<"\n-h | --help : Print (this) help message and exit";
  std::cout<<"\n-e [ref_ell]: Select reference ellipsoid; can be one of:";
  std::cout<<"\n\t * \"grs80\" (default)";
  std::cout<<"\n\t * \"wgs84\"";
  std::cout<<"\n\t * \"pz90\"";
  std::cout<<"\n-f [file]: Specify an input file";
  std::cout<<"\n-s [int n]: Skip n columns from the begining of each line";
  std::cout<<"\n-i : Perform the inverse computation, aka ellipsoidal to cartesian";
  std::cout<<"\n-idecdeg : Input is in decimal degrees";
  std::cout<<"\n-odecdeg : Output is in decimal degrees";
  std::cout<<"\n-ihexdeg : Input is in hexicondal degrees";
  std::cout<<"\n-ohexdeg : Output is in hexicondal degrees";
  std::cout<<"\n";
  return;
}

// Parse command line options to an std::map<str, str>
// This function will only collect and parse cmd and will not check if they
// are indeed valid.
// When using it (in main) do skip the first cmd as it is the name of the
// (called) program.
//
// Return:
// -1 -> help switch found; print help message and return
//  1 -> error parsing command line arguments
//  0 -> cmds resolved
int
cmd_parse(int argc, char* argv[], str_str_map& smap)
{
  for (int i=0; i<argc; i++) {
    // help message; print help and return
    if (!std::strcmp(argv[i], "-h") || !std::strcmp(argv[i], "--help")) {
      print_help();
      std::cout<<"\n";
      return -1;
    } 
    // ellipsoid selection
    else if (!std::strcmp(argv[i], "-e")) {
      if (i+1 >= argc) { 
        return 1;
      }
      smap["ellipsoid"] = std::string(argv[i+1]);
      ++i;
    }
    // inverse or not
    else if (!std::strcmp(argv[i], "-i")) {
      smap["inverse_cmp"] = "1";
    }
    // input file selection
    else if (!std::strcmp(argv[i], "-f")) {
      if (i+1 >= argc) { 
        return 1;
      }
      smap["ifile"] = std::string(argv[i+1]);
      ++i;
    }
    // input is in decimal degrees
    else if (!std::strcmp(argv[i], "-idecdeg")) {
      smap["in_is_decdeg"] = "1";
    }
    // input is in hexicondal degrees
    else if (!std::strcmp(argv[i], "-ihexdeg")) {
      smap["in_is_hexdeg"] = "1";
    }
    // output is in decimal degrees
    else if (!std::strcmp(argv[i], "-odecdeg")) {
      smap["out_is_decdeg"] = "1";
    }
    // output is in hexicondal degrees
    else if (!std::strcmp(argv[i], "-ohexdeg")) {
      smap["out_is_hexdeg"] = "1";
    }
    // skip nr of cols
    else if (!std::strcmp(argv[i], "-s")) {
      if (i+1 >= argc) { 
        return 1;
      }
      smap["skip_cols"] = std::string(argv[i+1]);
      ++i;
    }
    // what's that??
    else {
      std::cerr<<"\nIrrrelevant cmd: \"" << argv[i] << "\". Skipping!";
    }
  }
  return 0;
}

// split a std::string based on the delimeter sep. If more than one
// consecutive appearances of sep exist, they will not be treated as a token
// (e.g. "foo1 foo2        foo3    " will return "foo1", foo2", "foo3").
// The function will return a vector of strings.
std::vector<std::string>
tokenize(const std::string& str, char sep=' ')
{
  std::string::size_type start = 0,
                         stop = 0;
  std::vector<std::string> tokens;
  while ( (start = str.find_first_not_of(sep, stop)) != std::string::npos ) {
    stop = str.find_first_of(sep, start);
    if (stop == std::string::npos) stop = str.length();
    tokens.emplace_back(str, start, stop-start);
    stop+=1;
  }

  return tokens;
}

// Read a series of Points from any input stream; the function will fill
// the (passed in) points vector, assigning [x,y,z] values for each point.
// skip:    number of columns to skip from the begining of the line
// deg2rad: treat the coordinates as decimal degrees
// hex2rad: treat the coordinates as hexicondal degrees (aka each lat/lon 
// coordinate has three columns, degrees, minutes, seconds)
// The assigned coordinates (for each point), will be in units of meters or
// radians and meters.
// A returned value other than 0, signifies an error.
int
read_crd(std::istream& input, std::vector<Point>& points, int skip=0, 
    bool deg2rad=false, bool hex2deg=false)
{
  points.clear();
  std::vector<std::string> tokens;
  double x,y,z;
  std::string cmnt;

  // x, y and z are all one column-numbers; can be meters, radians or decimal
  // degrees
  if (!hex2deg) {
    for (std::string line; std::getline(input, line); ) {
      tokens = tokenize(line);
      try {
        x = std::stod(tokens[skip]);
        y = std::stod(tokens[skip+1]);
        z = std::stod(tokens[skip+2]);
        if (skip) {
          cmnt = "";
          for (int i=0; i<skip; i++) cmnt += tokens[i];
        }
      } catch (std::exception& e) {
        std::cerr<<"\n[ERROR] Failed to resolve line: \""<<line<<"\"";
        return 1;
      }
      points.emplace_back(x, y, z, cmnt);
    }
  } else {
    // x and y are in hexicondal degrees; transform to decimal deg.
    deg2rad = true;
    for (std::string line; std::getline(input, line); ) {
      tokens = tokenize(line);
      try {
        x = ngpt::hexd2decd<double>(std::stoi(tokens[skip+0]), 
          std::stoi(tokens[skip+1]),
          std::stod(tokens[skip+2]));
        y = ngpt::hexd2decd<double>(std::stoi(tokens[skip+3]),
          std::stoi(tokens[skip+4]),
          std::stod(tokens[skip+5]));
        z = std::stod(tokens[skip+6]);
        if (skip) {
          cmnt = "";
          for (int i=0; i<skip; i++) cmnt += tokens[i];
        }
      } catch (std::exception& e) {
        std::cerr<<"\n[ERROR] Failed to resolve line: \""<<line<<"\"";
        return 1;
      }
      points.emplace_back(x, y, z, cmnt);
    }
  }

  // transform to radians if needed (from decimal degrees)
  if (deg2rad) {
    for (auto& p : points) {
      p.x = ngpt::deg2rad<double>(p.x);
      p.y = ngpt::deg2rad<double>(p.y);
    }
  }

  return 0;
}

/// --------------------
/// Ellipsoids (-e):
///  * grs80
///  * wgs84
///  * pz90
/// --------------------
int
main(int argc, char* argv[])
{
  //  a dictionary with (any) default options
  str_str_map arg_dict;
  arg_dict["ellipsoid"]     = std::string("grs80");
  arg_dict["ifile"]         = std::string("");
  arg_dict["inverse_cmp"]   = "0",
  arg_dict["in_is_decdeg"]  = "0",
  arg_dict["in_is_hexdeg"]  = "0";
  arg_dict["out_is_decdeg"] = "0",
  arg_dict["out_is_hexdeg"] = "0";
  arg_dict["skip_cols"]     = "0";

  //  Parse command line arguments to the dictionary
  if (argc < 2) {
    print_help();
    return 1;
  }
  int status = cmd_parse(argc-1, argv+1, arg_dict);
  if (status > 0) {
    std::cerr<<"\n[ERROR] Invalid command line arguments.\n";
    return 1;
  } else if (status < 0) {
    return 0;
  }

  // Resolve boolean options to vars
  bool inverse_cmp   = arg_dict["inverse_cmp"]   == "1" ? true : false;
  bool in_is_decdeg  = arg_dict["in_is_decdeg"]  == "1" ? true : false;
  bool in_is_hexdeg  = arg_dict["in_is_hexdeg"]  == "1" ? true : false;
  bool out_is_decdeg = arg_dict["out_is_decdeg"] == "1" ? true : false;
  bool out_is_hexdeg = arg_dict["out_is_hexdeg"] == "1" ? true : false;
  // todo: need to check this!
  int  skip_cols    = std::stoi(arg_dict["skip_cols"]);
  assert( skip_cols >= 0 );

  // check that the ellipsoid selection is ok and assign it
  Ellipsoid Ell (ellipsoid::grs80);
  if (arg_dict["ellipsoid"] != "grs80") {
    ;
  } else if (arg_dict["ellipsoid"] != "wgs84") {
    Ell = ngpt::Ellipsoid(ellipsoid::wgs84);
  } else if (arg_dict["ellipsoid"] != "pz90") {
    Ell = ngpt::Ellipsoid(ellipsoid::pz90);
  } else {
    std::cerr<<"\n[ERROR] Invalid ellipsoid \""<<arg_dict["ellipsoid"]<<"\"";
    return 1;
  }

  // read input from stdin or file; assign points vector
  std::vector<Point> points;
  if (arg_dict["ifile"] != "") {
    std::ifstream ifile(arg_dict["ifile"]);
    if (!ifile.is_open()) {
      std::cerr<<"\n[ERROR] Could not open input file \""<< arg_dict["ifile"] <<"\"\n.";
      return 1;
    }
    if (read_crd(ifile, points, skip_cols, in_is_decdeg, in_is_hexdeg)) {
      return 1;
    }
  } else {
    if (read_crd(std::cin, points, skip_cols, in_is_decdeg, in_is_hexdeg)) {
      return 1;
    }
  }

  // perform the coordinate transformation
  if (!inverse_cmp) {
    for (auto& p : points) ngpt::car2ell(p.x, p.y, p.z, Ell, p.yr, p.xr, p.zr);
  } else {
    for (auto& p : points) ngpt::ell2car(p.y, p.x, p.z, Ell, p.xr, p.yr, p.zr);
  }

  // write results to stdout
  int max_ch = 0;
  for (const auto& p : points) if (p.info.size()>max_ch) max_ch = p.info.size();

  if (inverse_cmp) { // cartesian in meters
    for (const auto& p : points) 
      printf("%.*s %+15.5f %+15.5f %+15.5f\n", max_ch+1, p.info.c_str(), 
        p.xr, p.yr, p.zr);
  } else {
    if (out_is_hexdeg) { // ellipsoidal in hexicondal deg.
      int phi_deg, phi_min, lam_deg, lam_min;
      double phi_sec, lam_sec;
      for (const auto& p : points) {
        ngpt::rad2hexd<double>(p.yr, phi_deg, phi_min, phi_sec);
        ngpt::rad2hexd<double>(p.xr, lam_deg, lam_min, lam_sec);
        printf("%.*s %+03d %02d %10.7f %+03d %02d %10.7f %10.4f\n", 
          max_ch+1, p.info.c_str(), lam_deg, lam_min, lam_sec, 
          phi_deg, phi_min, phi_sec, p.zr);
      }
    } 
    else if (out_is_decdeg) { // ellipsoidal in decimal degrees
      for (const auto& p : points) 
        printf("%.*s %+15.10f %+15.10f %+10.4f\n", max_ch+1, p.info.c_str(), 
          ngpt::rad2deg<double>(p.xr), ngpt::rad2deg<double>(p.yr), p.zr);
    }
    else { // ellipsoidal in decimal radians
      for (const auto& p : points) 
        printf("%.*s %+15.10f %+15.10f %+10.4f\n", max_ch+1, p.info.c_str(), 
          p.xr, p.yr, p.zr);
    }
  }

  // all done!
  return 0;
}
