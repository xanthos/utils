#include <cstdio>
#include <iostream>

#include "ggdatetime/dtcalendar.hpp"

using ngpt::seconds;
using ngpt::datetime;
using ngpt::year;
using ngpt::month;
using ngpt::day_of_month;

constexpr long SEC_IN_DAY = 86400L;

void
print_help()
{
  std::cout<<"\nUsage: ymd2ydoy <YEAR> <MONTH> <DAY_OF_MONTH>";
  std::cout<<"\n";
  std::cout<<"\nTrnasform a given given in Year Month and Day_of_Month format";
  std::cout<<"\nYear, Day_of_Year format";
  std::cout<<"\n";
  std::cout<<"\nOptions:";
  std::cout<<"\n";
  return;
}

int 
main(int argc, char* argv[])
{
  //  Parse command line arguments 
  if (argc < 3) {
    print_help();
    return 1;
  }
  int iyr, im, idom;
  try {
    iyr  = std::stoi(argv[1]);
    im   = std::stoi(argv[2]);
    idom = std::stoi(argv[3]);
  } catch (std::exception& e) {
    std::cerr<<"\n[ERROR] Failed to resolve input date!";
    return 1;
  }

  year yr (iyr);
  month mn (im);
  day_of_month dm (idom);
  if (iyr<=0 || (!mn.is_valid() || !dm.is_valid(yr, mn))) {
    std::cerr<<"\n[ERROR] Invalid input date!";
    return 1;
  }
  seconds sec (1);
  datetime<seconds> date (yr, mn, dm, sec);
  
  auto ydoy = date.as_ydoy();
  int oyr (ydoy.__year.as_underlying_type());
  int ody (ydoy.__doy.as_underlying_type());
  if (oyr!=iyr) {
    std::cerr<<"\n[ERROR] Failed to transform input date";
    return 1;
  }
  
  printf("%04d %03d\n", oyr, ody);

  return 0;
}
