#include <cstdio>
#include <iostream>

#include "ggdatetime/dtcalendar.hpp"

using ngpt::seconds;
using ngpt::datetime;
using ngpt::year;
using ngpt::day_of_year;
using ngpt::gps_week;
using ngpt::ydoy_date;

constexpr long SEC_IN_DAY = 86400L;

void
print_help()
{
  std::cout<<"\nUsage: gwk2ydoy <GPS_WEEK> <DAY_OF_WEEK>";
  std::cout<<"\n";
  std::cout<<"\nTransform a given given in Gps_Week and Day_of_Week format to";
  std::cout<<"\nYear Day_Of_Year format";
  std::cout<<"\n";
  std::cout<<"\nOptions:";
  std::cout<<"\n";
  return;
}

int 
main(int argc, char* argv[])
{
  //  Parse command line arguments 
  if (argc < 2) {
    print_help();
    return 1;
  }
  int igwk, igdow;
  try {
    igwk  = std::stoi(argv[1]);
    igdow = std::stoi(argv[2]);
  } catch (std::exception& e) {
    std::cerr<<"\n[ERROR] Failed to resolve input date!";
    return 1;
  }

  if (igdow<0 || igdow>6) {
    std::cerr<<"\n[ERROR] Invalid input date!";
    return 1;
  }

  gps_week gwk  (igwk);
  seconds  gsow (static_cast<long>(igdow)*SEC_IN_DAY);
  datetime<seconds> date (gwk, gsow);
  
  ydoy_date ydoy (date.as_ydoy());
  int iyear (static_cast<int>(ydoy.__year.as_underlying_type())),
      idoy  (static_cast<int>(ydoy.__doy.as_underlying_type()));
  printf("%4d %03d\n", iyear, idoy);

  return 0;
}
