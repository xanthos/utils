#include <cstdio>
#include <iostream>

#include "ggdatetime/dtcalendar.hpp"

using ngpt::seconds;
using ngpt::datetime;
using ngpt::year;
using ngpt::day_of_year;
using ngpt::gps_week;

constexpr long SEC_IN_DAY = 86400L;

void
print_help()
{
  std::cout<<"\nUsage: ydoy2gwk <YEAR> <DOY>";
  std::cout<<"\n";
  std::cout<<"\nTrnasform a given given in Year Day_of_Year format to Gps Week";
  std::cout<<"\nand Day_of_Week format.";
  std::cout<<"\n";
  std::cout<<"\nOptions:";
  std::cout<<"\n";
  return;
}

int 
main(int argc, char* argv[])
{
  //  Parse command line arguments 
  if (argc < 2) {
    print_help();
    return 1;
  }
  int iyr, idoy;
  try {
    iyr  = std::stoi(argv[1]);
    idoy = std::stoi(argv[2]);
  } catch (std::exception& e) {
    std::cerr<<"\n[ERROR] Failed to resolve input date!";
    return 1;
  }

  if (iyr<=0 || (idoy<1 || idoy>366)) {
    std::cerr<<"\n[ERROR] Invalid input date!";
    return 1;
  }

  year yr (iyr);
  day_of_year doy (idoy);
  seconds sec (1);
  datetime<seconds> date (yr, doy, sec);
  
  long sec_of_week;
  gps_week week;
  week = date.as_gps_wsow(sec_of_week);
  long day_of_week = sec_of_week / SEC_IN_DAY;
  if (sec_of_week > 7*SEC_IN_DAY || day_of_week > 6) {
    std::cerr<<"\n[ERROR] Failed to transform date";
    return 1;
  }
  int iweek (static_cast<int>(week.as_underlying_type())),
      idow  (static_cast<int>(day_of_week));
  printf("%04d %1d\n", iweek, idow);

  return 0;
}
