#include <cstdio>
#include <iostream>

#include "ggdatetime/dtcalendar.hpp"

using ngpt::seconds;
using ngpt::datetime;
using ngpt::year;
using ngpt::day_of_year;
using ngpt::gps_week;

constexpr long SEC_IN_DAY = 86400L;

void
print_help()
{
  std::cout<<"\nUsage: ydoy2ymd <YEAR> <DOY>";
  std::cout<<"\n";
  std::cout<<"\nTrnasform a given given in Year Day_of_Year format to Year, Month";
  std::cout<<"\nand Day_of_Month format.";
  std::cout<<"\n";
  std::cout<<"\nOptions:";
  std::cout<<"\n";
  return;
}

int 
main(int argc, char* argv[])
{
  //  Parse command line arguments 
  if (argc < 2) {
    print_help();
    return 1;
  }
  int iyr, idoy;
  try {
    iyr  = std::stoi(argv[1]);
    idoy = std::stoi(argv[2]);
  } catch (std::exception& e) {
    std::cerr<<"\n[ERROR] Failed to resolve input date!";
    return 1;
  }

  if (iyr<=0 || (idoy<1 || idoy>366)) {
    std::cerr<<"\n[ERROR] Invalid input date!";
    return 1;
  }

  year yr (iyr);
  day_of_year doy (idoy);
  seconds sec (1);
  datetime<seconds> date (yr, doy, sec);
  
  auto ymd = date.as_ymd();
  int oyr (ymd.__year.as_underlying_type());
  int omn (ymd.__month.as_underlying_type());
  int odm (ymd.__dom.as_underlying_type());
  if (oyr!=iyr || !ymd.is_valid()) {
    std::cerr<<"\n[ERROR] Failed to transform input date";
    return 1;
  }
  
  printf("%04d %02d %02d\n", oyr, omn, odm);

  return 0;
}
