#include <cstdio>
#include <iostream>
#include <cstring>
#include <vector>
#include "ggdatetime/dtcalendar.hpp"

using ngpt::datetime;
using ngpt::year;
using ngpt::month;
using ngpt::day_of_month;
using ngpt::day_of_year;
using ngpt::modified_julian_day;
using ngpt::gps_week;
using ngpt::seconds;

constexpr long SEC_IN_DAY = 86400L;

enum InputFormat { YMD=0, YOD, YDOY, GWK, MJD };

int
str2format(const char* str)
{
  std::string inpt (str);
  int selection = 0;
  std::vector<std::string> formats = {"ymd", "yod", "ydoy", "gwk", "mjd"};
  for (const auto& s : formats) {
    if (s==inpt) return selection;
    selection++;
  }
  return selection;
}

datetime<seconds>
resolve_input(InputFormat inpf, const std::vector<std::string>& args)
{
  int i1, i2, i3;
  float f1;
  switch (inpf) {
    case YMD:
      assert(args.size()==3);
      i1 = std::stoi(args[0]);
      i2 = std::stoi(args[1]);
      i3 = std::stoi(args[2]);
      return datetime<seconds>(year(i1), month(i2), day_of_month(i3), seconds(1));
      break;
    case YOD:
      assert(args.size()==3);
      i1 = std::stoi(args[0]);
      //month mn(args[1].c_str());
      i3 = std::stoi(args[2]);
      return datetime<seconds>(year(i1), month(args[1].c_str()), day_of_month(i3), seconds(1));
      break;
    case YDOY:
      assert(args.size()==2);
      i1 = std::stoi(args[0]);
      i2 = std::stoi(args[1]);
      return datetime<seconds>(year(i1), day_of_year(i2), seconds(1));
      break;
    case GWK:
      assert(args.size()==2);
      i1 = std::stoi(args[0]);
      i2 = std::stoi(args[1]);
      //long sow = i2*SEC_IN_DAY;
      return datetime<seconds>(gps_week(i1), seconds(i2*SEC_IN_DAY));
      break;
    case MJD:
      assert(args.size()==1);
      f1 = std::stof(args[0]); 
      return datetime<seconds>(modified_julian_day(f1), seconds(1));
      break;
  }
  return datetime<seconds>();
}

void
print_help()
{
  std::cout<<"\nUsage: calendar [FORMAT] [DATE]";
  std::cout<<"\n";
  std::cout<<"\nTrnasform a given given in Year Month and Day_of_Month format";
  std::cout<<"\nYear, Day_of_Year format";
  std::cout<<"\n";
  std::cout<<"\nOptions:";
  std::cout<<"\n[FORMAT] can be any of the following:";
  std::cout<<"\n  * ymd  meaning <YEAR> <MONTH> <DAY OF MONTH>";
  std::cout<<"\n  * yod  meaning <YEAR> <MONTH> <DAY OF MONTH>, where MONTH is";
  std::cout<<"\n         given as a string e.g. \"June\" or \"Jun\"";
  std::cout<<"\n  * ydoy meaning <YEAR> <DAY_OF_YEAR>";
  std::cout<<"\n  * gwk  meaning <GPS_WEEK> <DAY_OF_WEEK>";
  std::cout<<"\n  * mjd  meaning <MODIFIED_JULIAN_DAY>";
  std::cout<<"\n";
  return;
}

int 
main(int argc, char* argv[])
{
  //  Parse command line arguments 
  if (argc < 2) {
    print_help();
    return 1;
  }
  // Resolve the (input) format identifier
  int format = str2format(argv[1]);
  if (format>4) {
    std::cerr<<"\n[ERROR] Invalid Format Identifier (\""<<argv[1]<<"\")";
    return 1;
  }
  // Collect date-relevant cmd's and resolve the input date
  std::vector<std::string> cargv;
  for (int i=2; i<argc; i++) cargv.emplace_back(argv[i]);
  auto date = resolve_input(static_cast<InputFormat>(format), cargv);

  // Now transform date to all formats and print it
  auto ymd = date.as_ymd();
  int iyr (ymd.__year.as_underlying_type());
  int imn (ymd.__month.as_underlying_type());
  int idm (ymd.__dom.as_underlying_type());
  if (!ymd.is_valid()) {
    std::cerr<<"\n[ERROR] Input date seems erroneous";
    return 1;
  }
  printf("%5s %04d %02d %02d\n", "YMD", iyr, imn, idm);

  auto ydoy = date.as_ydoy();
  int _iyr (ydoy.__year.as_underlying_type());
  int idoy (ydoy.__doy.as_underlying_type());
  assert(_iyr==iyr);
  printf("%5s %04d %03d\n", "YDOY", iyr, idoy);

  long sec_of_week;
  gps_week week (date.as_gps_wsow(sec_of_week));
  long day_of_week = sec_of_week / SEC_IN_DAY;
  if (sec_of_week > 7*SEC_IN_DAY || day_of_week > 6) {
    std::cerr<<"\n[ERROR] Failed to transform date to GPS Week";
    return 1;
  }
  int iweek (static_cast<int>(week.as_underlying_type())),
      idow  (static_cast<int>(day_of_week));
  printf("%5s %04d %1d\n", "GWK", iweek, idow);

  double dmjd = date.as_mjd();
  printf("%5s %8.1f\n", "MJD", dmjd);

  return 0;
}
